---
title: First Post
date: 09.02.2022
slug: first-post
---
# amogus
1. a
2. m
3. o
4. g
5. u
6. s

```rust
fn main() { println!("Hello World!") }
```

```rust
async fn async_function() -> i32 {
    /* do async stuff */
    0
}

fn main() {
    let returned_value = futures::executor::block_on(async_function());
    assert_eq!(returned_value, 0);
}
```

```ts

interface Post {
    title: string
    content: string
}

const post: Post;

```
![Amogus](./Amogus_Template.png)
